#include <stdio.h>
#include <stdlib.h>

struct Mark {
	int x;
	int y;
};

typedef struct Mark Mark;

/* load the structure that p points to with the date from f */
void getInfo (FILE * f, Mark * p);

/* write the data stored in structure item into output file f */
void printInfo (FILE * f, Mark item);

/* compare what pointers a and b point to; to be used by qsort() */
int compare (const void * a, const void * b);

int main()
{
	Mark list[100];
	Mark mark;
	int size = 0, i, col = 0;
	FILE * fin;

	fin = fopen ("lab5.dat", "r");
	if ( fin == NULL )
	{
		printf ("Cannot open file.\n");
		exit(1);
	}

	while ( !feof(fin) )
	{
		getInfo (fin, &mark);
		list[size++] = mark;
	}

	// use qsort() to sort data in list
    qsort(list, size, sizeof(int)*2, compare);

    for (i = 0; i < size; i++)
	{
		printInfo (stdout, list[i]);
		if ( ++col % 5 == 0 )
			printf("\n");
	}
    printf("\n");
	fclose (fin);

	return 0;
}

/* complete the following helper functions */

void getInfo (FILE * f, Mark * p)
{
    int bytes_read;
    int nbytes = 100;
    char *line;

    line = (char *) malloc (nbytes+1);
    bytes_read = getline (&line, &nbytes, f);

    if(bytes_read == -1){
        puts ("Error reading line!");
    }else{
        
        char *token = strtok(line, " ");
        p->x = atoi(&token[0]);
        token = strtok(NULL, " ");
        p->y = atoi(&token[0]);

        //printf("x: %d y: %d\n", p->x,p->y);
    }
}

void printInfo (FILE * f, Mark item)
{
	// display each mark in format of (x, y) 
	// and five marks per line 

    fprintf(f, " (%d,%d) ", item.x, item.y);

}

int compare (const void * a, const void * b)
{
    const struct Mark *aa = a;
    const struct Mark *bb = b;
    
    if (aa->y == bb->y){
        if(aa->x > bb->x){return 1;}
        else if(aa->x < bb->x){ return -1;}
        else{return 0;}
    }else{
        if (aa->y < bb->y){return 1;}
        if (aa->y > bb->y){return -1;}
    }
}


